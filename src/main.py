import argparse
import settings
import tarfile
import os
import re

def main():
    args     = get_arguments()
    parent   = 'dist'
    filename = f'{args.name}-{args.os}-{args.arch}.tar.gz'
    filepath = os.path.join(parent, filename)
    os.makedirs(parent, exist_ok=True)
    verify(args)
    create(filepath)

def get_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('name', help='Package name')
    parser.add_argument('os',   help=f'Operating system {settings.os}')
    parser.add_argument('arch', help=f'Architecture {settings.arch}')
    return parser.parse_args()

def verify(args):
    verify_package_name(args.name)
    verify_os(args.os)
    verify_arch(args.arch)
    verify_directories()
    verify_files()

def verify_package_name(name):
    pattern = '^([a-z][a-z0-9]*)(.[a-z][a-z0-9]*)*$'
    program = re.compile(pattern)
    assert program.match(name), name

def verify_os(name):
    assert name in settings.os, name

def verify_arch(name):
    assert name in settings.arch, name

def verify_directories():
    for directory in settings.dirs:
        if os.path.exists(directory):
            assert os.path.isdir(directory), directory

def verify_files():
    for file in settings.files:
        if os.path.exists(file):
            assert os.path.isfile(file), file

def create(filepath):
    with tarfile.open(filepath, 'w:gz') as tar:
        for file in settings.dirs + settings.files:
            if os.path.exists(file):
                tar.add(file)

main()
