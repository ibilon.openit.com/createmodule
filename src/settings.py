dirs  = ['bin', 'sbin', 'lib', 'etc', 'var', 'opt', 'tmp']
files = ['requirements.txt']
os    = ['windows', 'linux', 'darwin', 'solaris']
arch  = ['386', 'amd64', 'arm', 'arm64', 'sparc', 'sparc64']
