@echo off
set root=%~dp0\..
cd "%root%"
rmdir /s /q dist
rmdir /s /q build
del /s /q *.spec
python -m pip install -r requirements.txt --upgrade
python -m PyInstaller --onefile "%root%\src\main.py" --name openit_createmodule --icon "%root%\icon.ico"
